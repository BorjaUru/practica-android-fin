package com.bulsa.farmaciaszgz;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class ListaFarmaciasActivity extends AppCompatActivity {
    private ListView lista;
    public static ArrayList<Farmacia> listaff;
    public static FarmaciaAdapter adaptador;


    private Database d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_farmacias);


        lista = (ListView) findViewById(R.id.lFarmacias);
        listaff =new ArrayList<>();
        d=new Database(this);


        actualizarJson();



        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Farmacia f=(Farmacia)adaptador.getItem(i);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(f.getUrl()));
                startActivity(intent);
            }
        });
        registerForContextMenu(lista);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_lista_farmacias, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.cerrar:
                intent= new Intent(ListaFarmaciasActivity.this, MainActivity.class);
                startActivity(intent);
                return true;
            case R.id.informacion:
                intent= new Intent(ListaFarmaciasActivity.this, AcercaDeActivity.class);
                startActivity(intent);
                return true;
            case R.id.favoritos:
                intent= new Intent(ListaFarmaciasActivity.this, ListaFarmaciaFActivity.class);
                startActivity(intent);
                return true;
            case R.id.mapas:
                intent= new Intent(ListaFarmaciasActivity.this, MapaActivity.class);
                intent.putExtra("favoritos", "a");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.contex_item_lista_farmacias1, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final int itemSeleccionado = info.position;

        Farmacia f;
        switch (item.getItemId()) {
            case R.id.Cmapa:
                Intent intentMapa = new Intent(this, Mapa1FActivity.class);
                f= listaff.get(itemSeleccionado);
                intentMapa.putExtra("nombre", f.getNombre());
                intentMapa.putExtra("latitud", f.getLatitud());
                intentMapa.putExtra("longitud", f.getLongitud());
                startActivity(intentMapa);
                break;
            case R.id.Cfavoritos:
                f= listaff.get(itemSeleccionado);
                d.nuevaF(f);
                break;
            default:
                break;
        }
        return false;
    }



    @Override
    protected void onResume() {
        super.onResume();

        actualizarJson();

    }


    public void actualizarJson(){
        listaff =new ArrayList<>();
        adaptador=new FarmaciaAdapter(this, listaff);
        TareaDescarga tarea = new TareaDescarga(this);
        tarea.execute();

        lista.setAdapter(adaptador);
    }

}