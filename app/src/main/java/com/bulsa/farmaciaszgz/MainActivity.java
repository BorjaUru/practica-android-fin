package com.bulsa.farmaciaszgz;


import android.content.DialogInterface;
import android.content.Intent;

import android.os.AsyncTask;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


import static com.bulsa.farmaciaszgz.util.Constantes.SERVER_URL;

public class MainActivity extends AppCompatActivity {
    private EditText user;
    private EditText pass;
    private CheckBox aa;
    private Button login;
    private Button registrar;

    private Boolean a;
    private String usuario;
    private String pas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        aa=(CheckBox)findViewById(R.id.checkBox) ;

        user=(EditText)findViewById(R.id.etUsuario);
        pass=(EditText)findViewById(R.id.etPass);

        login=(Button)findViewById(R.id.btAceptar);
        registrar=(Button)findViewById(R.id.btRegistrar);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(aa.isChecked()){
                    Intent i= new Intent(MainActivity.this, ListaFarmaciasActivity.class);
                    startActivity(i);
                }else {
                    usuario = user.getText().toString();
                    pas = pass.getText().toString();
                    WebService webService = new WebService();
                    webService.execute(usuario, pas);

                }
            }
        });
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i= new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    private class WebService extends AsyncTask<String, Void, Void> {

        boolean a;

        @Override
        protected Void doInBackground(String... params) {

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            a = restTemplate.getForObject(SERVER_URL + "/usuario_login?nombre="+params[0]+"&pass="+params[1], Boolean.class);


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();




        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(a) {
                Intent i = new Intent(MainActivity.this, ListaFarmaciasActivity.class);
                startActivity(i);
            }else {

                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                "email o pass erroneos", Toast.LENGTH_SHORT);

                toast1.show();
            }


        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);

        }
    }
}
