package com.bulsa.farmaciaszgz;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by borja on 20/12/2016.
 */

public class ComentarioAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Comentario> listaComentarios;
    private LayoutInflater inflater;

    public ComentarioAdapter(Activity context, ArrayList<Comentario> listaComentarios) {
        this.context = context;
        this.listaComentarios = listaComentarios;
        inflater = LayoutInflater.from(context);
    }

    static class ViewHolder {
        TextView nota;
        TextView descripcion;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ComentarioAdapter.ViewHolder holder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_lista_comentarios, null);

            holder = new ViewHolder();
            holder.nota = (TextView) convertView.findViewById(R.id.tvNota);
            holder.descripcion = (TextView) convertView.findViewById(R.id.tvDescripcion);
            convertView.setTag(holder);
        }
        else {
            holder = (ComentarioAdapter.ViewHolder) convertView.getTag();
        }

        Comentario c = listaComentarios.get(position);
        holder.nota.setText(c.getNota());
        holder.descripcion.setText(c.getDescripcion());
        return convertView;
    }

    @Override
    public int getCount() {
        return listaComentarios.size();
    }

    @Override
    public Object getItem(int posicion) {
        return listaComentarios.get(posicion);
    }

    @Override
    public long getItemId(int posicion) {
        return posicion;
    }

}
