package com.bulsa.farmaciaszgz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ModificarFActivity extends AppCompatActivity {
    private EditText urll;
    private EditText telefonoo;
    private EditText direccionn;
    private Button aceptar;
    private Button cancelar;
    private Farmacia f;
    Database d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_f);
        d=new Database(this);
        f=new Farmacia();
        urll=(EditText)findViewById(R.id.editText5);
        telefonoo=(EditText)findViewById(R.id.editText6);
        direccionn=(EditText)findViewById(R.id.editText7);
        aceptar=(Button)findViewById(R.id.button3);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                f.setDireccion(direccionn.getText().toString());
                f.setTelefono(telefonoo.getText().toString());
                f.setUrl(urll.getText().toString());
                d.modificarEvento(f);
               Intent intent= new Intent(ModificarFActivity.this, ListaFarmaciaFActivity.class);
                startActivity(intent);
            }
        });
        cancelar=(Button)findViewById(R.id.button4);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(ModificarFActivity.this, ListaFarmaciaFActivity.class);
                startActivity(intent);
            }
        });
        Intent intent = getIntent();

        String nombre = intent.getStringExtra("a");
        String url = intent.getStringExtra("aa");
        String telefono = intent.getStringExtra("aaa");
        double latitud = intent.getDoubleExtra("aaaa",0);
        double longitud = intent.getDoubleExtra("aaaaa",0);
        String direccion = intent.getStringExtra("aaaaaa");
        f.setNombre(nombre);
        f.setUrl(url);
        f.setTelefono(telefono);
        f.setLatitud(latitud);
        f.setLongitud(longitud);
        f.setDireccion(direccion);
        urll.setText(f.getUrl());
        telefonoo.setText(f.getTelefono());
        direccionn.setText(f.getDireccion());
    }
}
